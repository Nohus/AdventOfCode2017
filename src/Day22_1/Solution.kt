package Day22_1

import Day22_1.Dir.*
import solveInputs

enum class Dir {

    UP, RIGHT, DOWN, LEFT;

    fun turnRight(): Dir {
        return when(this) {
            UP -> RIGHT
            RIGHT -> DOWN
            DOWN -> LEFT
            LEFT -> UP
        }
    }

    fun turnLeft(): Dir {
        return when(this) {
            UP -> LEFT
            RIGHT -> UP
            DOWN -> RIGHT
            LEFT -> DOWN
        }
    }

}

data class Pos(val x: Int, val y: Int) {

    fun forward(dir: Dir): Pos {
        return when(dir) {
            UP -> Pos(x, y - 1)
            RIGHT -> Pos(x + 1, y)
            DOWN -> Pos(x, y + 1)
            LEFT -> Pos(x - 1, y)
        }
    }

}

val map = mutableMapOf<Pos, Boolean>()
        .withDefault { false }

fun main(args: Array<String>) {
    solveInputs { input ->
        map.clear()
        val offset = input.lines().size / 2
        input.lines().forEachIndexed { y, row ->
            row.chunked(1).forEachIndexed { x, s ->
                if (s == "#") {
                    map[Pos(x - offset, y - offset)] = true
                }
            }
        }

        var pos = Pos(0, 0)
        var direction = UP
        var infections = 0
        repeat(10000) {
            direction = if (map.getValue(pos)) {
                map.remove(pos)
                direction.turnRight()
            } else {
                map[pos] = true
                infections++
                direction.turnLeft()
            }
            pos = pos.forward(direction)
        }

        "$infections"
    }
}
