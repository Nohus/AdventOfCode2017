package Day14_2

import solveInputs

fun hash(input: String): String {
    val list = (0..255).toMutableList()
    val newList = mutableListOf<Int>()
    var current = 0
    var skipSize = 0
    val bytes = input.toCharArray().map { it.toByte() }.toMutableList()
    bytes.addAll(listOf(17, 31, 73, 47, 23))
    for (i in 1..64) {
        bytes.forEach { length ->
            newList.clear()
            val endIndex = current + length
            if (endIndex > list.size) {
                val overflow = endIndex - list.size
                val reversed = (list.subList(current, list.size) + list.subList(0, overflow)).reversed()
                newList.apply {
                    addAll(reversed.subList(reversed.size - overflow, reversed.size)) // End of reversed fragment
                    addAll(list.subList(overflow, current)) // Between
                    addAll(reversed.subList(0, reversed.size - overflow)) // Start of reversed fragment
                }
            } else {
                newList.apply {
                    addAll(list.subList(0, current)) // Before reversed fragment
                    addAll(list.subList(current, current + length).reversed()) // Reversed
                    addAll(list.subList(current + length, list.size)) // After reversed fragment
                }
            }
            list.clear()
            list.addAll(newList)
            current += (length + skipSize)
            current -= list.size * (current / list.size)
            skipSize++
        }
    }
    return list.chunked(16).map { block ->
        block.takeLast(15).fold(block[0], { result, next -> result xor next})
    }.joinToString("") { String.format("%02x", it) }
}

data class Pos(val x: Int, val y: Int) {

    fun neighbours(): List<Pos> {
        val neighbours = mutableListOf<Pos>()
        if (x > 0) {
            neighbours += Pos(x - 1, y)
        }
        if (x < 127) {
            neighbours += Pos(x + 1, y)
        }
        if (y > 0) {
            neighbours += Pos(x, y - 1)
        }
        if (y < 127) {
            neighbours += Pos(x, y + 1)
        }
        return neighbours
    }

}

fun symbolAt(pos: Pos): String {
    return rows[pos.y][pos.x]
}

fun writeAt(pos: Pos, symbol: String) {
    rows[pos.y][pos.x] = symbol
}

fun findUngrouped(): Pos? {
    for (y in 0..127) {
        for (x in 0..127) {
            val pos = Pos(x, y)
            if (symbolAt(pos) == "X") {
                return pos
            }
        }
    }
    return null
}

fun floodFill(startPos: Pos, fill: String) {
    val list = mutableListOf<Pos>()
    list += startPos
    while (list.isNotEmpty()) {
        val pos = list.removeAt(0)
        writeAt(pos, fill)
        list += pos.neighbours().filter { symbolAt(it) == "X" }
        val set = list.toSet()
        list.clear()
        list.addAll(set)
    }
}

val rows = mutableListOf<MutableList<String>>()

fun main(args: Array<String>) {
    solveInputs { input ->
        rows.clear()
        for (i in 0..127) {
            val hash = hash("$input-$i")
            val binHash = hash.chunked(1)
                    .map { String.format("%4s", Integer.toBinaryString(it.toInt(16)))}
                    .map { it.replace(" ", "0") }
                    .map { it.replace("1", "X") }
                    .joinToString("").chunked(1).toMutableList()
            rows += binHash
        }
        println(rows)
        var count = 0
        while (true) {
            val ungrouped = findUngrouped() ?: break
            count++
            floodFill(ungrouped, count.toString())
        }
        println(rows)
        "$count"
    }
}
