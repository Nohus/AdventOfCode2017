package Day22_2

import Day22_2.Dir.*
import Day22_2.Node.*
import solveInputs

enum class Dir {

    UP, RIGHT, DOWN, LEFT;

    fun turnRight(): Dir {
        return when(this) {
            UP -> RIGHT
            RIGHT -> DOWN
            DOWN -> LEFT
            LEFT -> UP
        }
    }

    fun turnLeft(): Dir {
        return when(this) {
            UP -> LEFT
            RIGHT -> UP
            DOWN -> RIGHT
            LEFT -> DOWN
        }
    }

}

enum class Node {
    CLEAN, WEAKENED, INFECTED, FLAGGED
}

data class Pos(val x: Int, val y: Int) {

    fun forward(dir: Dir): Pos {
        return when(dir) {
            UP -> Pos(x, y - 1)
            RIGHT -> Pos(x + 1, y)
            DOWN -> Pos(x, y + 1)
            LEFT -> Pos(x - 1, y)
        }
    }

}

val map = mutableMapOf<Pos, Node>()
        .withDefault { CLEAN }

fun main(args: Array<String>) {
    solveInputs { input ->
        map.clear()
        val offset = input.lines().size / 2
        input.lines().forEachIndexed { y, row ->
            row.chunked(1).forEachIndexed { x, node ->
                if (node == "#") {
                    map[Pos(x - offset, y - offset)] = INFECTED
                }
            }
        }

        var pos = Pos(0, 0)
        var direction = UP
        var infections = 0
        repeat(10000000) {
            when (map.getValue(pos)) {
                CLEAN -> {
                    direction = direction.turnLeft()
                    map[pos] = WEAKENED
                }
                WEAKENED -> {
                    map[pos] = INFECTED
                    infections++
                }
                INFECTED -> {
                    direction = direction.turnRight()
                    map[pos] = FLAGGED
                }
                FLAGGED -> {
                    direction = direction.turnRight()
                    direction = direction.turnRight()
                    map.remove(pos)
                }
            }
            pos = pos.forward(direction)
        }

        "$infections"
    }
}
