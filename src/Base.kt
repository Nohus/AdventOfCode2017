import java.io.File
import java.util.*

private fun getInputs(): List<String> {
    val list = mutableListOf<String>()
    val name = Throwable().stackTrace[2].className.split(".")[0]
    val scanner = Scanner(File("src/$name/input"))
    val input = mutableListOf<String>()
    while (scanner.hasNextLine()) {
        val line = scanner.nextLine()
        if (line.isNotEmpty()) {
            input += line
        } else {
            list += input.joinToString("\n")
            input.clear()
        }
    }
    list += input.joinToString("\n")
    return list
}

fun solveInputs(solve: (String) -> String) {
    getInputs().forEach {
        println("==========")
        if (it.contains("\n")) {
            println("In:\n$it")
        } else {
            println("In: $it")
        }
        println("Out: ${solve(it)}")
    }
}
