package Day2_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val sum = input.lines().map {
            return@map it.split("\t").map { it.toInt() }.let {
                return@let it.max()!! - it.min()!!
            }
        }.sum()
        "$sum"
    }
}
