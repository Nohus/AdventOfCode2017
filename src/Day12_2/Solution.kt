package Day12_2

import solveInputs

val pipes = mutableMapOf<Int, MutableSet<Int>>()

fun addConnection(a: Int, b: Int) {
    val list = pipes[a] ?: mutableSetOf()
    list += b
    pipes[a] = list
}

fun getGroupWith(number: Int): Set<Int> {
    val expanded = mutableSetOf(number)
    val connections = pipes[number]!!.toMutableSet()
    while (connections.isNotEmpty()) {
        connections.toSet().forEach {
            pipes[it]!!.filterNot { it in expanded }.forEach {
                connections += it
            }
            expanded += it
            connections -= it
        }
    }
    return expanded
}

fun main(args: Array<String>) {
    solveInputs { input ->
        pipes.clear()
        input.lines().forEach {
            val split = it.split(" <-> ")
            val num = split[0].toInt()
            split[1].split(", ").map(String::toInt).forEach {
                addConnection(num, it)
            }
        }
        val groups = mutableSetOf<Set<Int>>()
        pipes.keys.forEach {
            groups += getGroupWith(it)
        }
        "${groups.size}"
    }
}
