package Day10_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val list = (0..255).toMutableList()
        val newList = mutableListOf<Int>()
        var current = 0
        var skipSize = 0
        input.split(",").map { it.toInt() }.forEach { length ->
            newList.clear()
            if (current + length > list.size) {
                val overflow = (current + length) - list.size
                val reversed = (list.subList(current, list.size) + list.subList(0, overflow)).reversed()
                newList.apply {
                    addAll(reversed.subList(reversed.size - overflow, reversed.size)) // End of reversed fragment
                    addAll(list.subList(overflow, current)) // Between
                    addAll(reversed.subList(0, reversed.size - overflow)) // Start of reversed fragment
                }
            } else {
                newList.apply {
                    addAll(list.subList(0, current)) // Before reversed fragment
                    addAll(list.subList(current, current + length).reversed()) // Reversed
                    addAll(list.subList(current + length, list.size)) // After reversed fragment
                }
            }
            list.clear()
            list.addAll(newList)
            current += (length + skipSize)
            current -= list.size * (current / list.size)
            skipSize++
        }
        val answer = list[0] * list[1]
        "$answer"
    }
}
