package Day11_1

import Day11_1.Dir.*
import solveInputs
import kotlin.math.abs

enum class Dir {
    NORTH_WEST, NORTH, NORTH_EAST, SOUTH_WEST, SOUTH, SOUTH_EAST
}

fun String.toDir(): Dir {
    return when(this) {
        "nw" -> NORTH_WEST
        "n" -> NORTH
        "ne" -> NORTH_EAST
        "sw" -> SOUTH_WEST
        "s" -> SOUTH
        "se" -> SOUTH_EAST
        else -> throw Exception()
    }
}

data class Pos(val x: Int, val y: Int) {

    private val even = (x % 2 == 0)

    fun moved(dir: Dir): Pos {
        return when(even) {
            true -> {
                when(dir) {
                    NORTH_WEST -> Pos(x - 1, y)
                    NORTH -> Pos(x, y - 1)
                    NORTH_EAST -> Pos(x + 1, y)
                    SOUTH_WEST -> Pos(x - 1, y + 1)
                    SOUTH -> Pos(x, y + 1)
                    SOUTH_EAST -> Pos(x + 1, y + 1)
                }
            }
            false -> {
                when(dir) {
                    NORTH_WEST -> Pos(x - 1, y - 1)
                    NORTH -> Pos(x, y - 1)
                    NORTH_EAST -> Pos(x + 1, y - 1)
                    SOUTH_WEST -> Pos(x - 1, y)
                    SOUTH -> Pos(x, y + 1)
                    SOUTH_EAST -> Pos(x + 1, y)
                }
            }
        }
    }

    fun toCube(): Cube {
        val x = this.x
        val z = this.y - (this.x + (this.x and 1)) / 2
        val y = -x - z
        return Cube(x, y, z)
    }

}

data class Cube(val x: Int, val y: Int, val z: Int)

fun cubeDistance(a: Cube, b: Cube): Int {
    return maxOf(abs(a.x - b.x), abs(a.y - b.y), abs(a.z - b.z))
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var current = Pos(0, 0)
        input.split(",").map(String::toDir).forEach {
            current = current.moved(it)
        }
        val distance = cubeDistance(Pos(0, 0).toCube(), current.toCube())
        "$distance"
    }
}
