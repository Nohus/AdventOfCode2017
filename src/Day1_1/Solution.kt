package Day1_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val numberList = input.chunked(1).map { it.toInt() }
        val sum = numberList.filterIndexed { index, number ->
            if (index < numberList.lastIndex) {
                number == numberList[index + 1]
            } else {
                number == numberList[0]
            }
        }.sum()
        "$sum"
    }
}
