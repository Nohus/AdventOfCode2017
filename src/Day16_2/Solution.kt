package Day16_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val moves = input.split(",")
        val original = "abcdefghijklmnop".chunked(1)
        var programs = original.toMutableList()
        val maxDances = 1000000000
        var dances = 1
        while (dances <= maxDances) {
            moves.forEach {
                val action = it.takeLast(it.length - 1)
                when {
                    it.startsWith("s") -> {
                        val number = action.toInt() % programs.size
                        val newList = programs.takeLast(number) + programs.take(programs.size - number)
                        programs = newList.toMutableList()
                    }
                    it.startsWith("x") -> {
                        val indexA = action.split("/")[0].toInt()
                        val indexB = action.split("/")[1].toInt()
                        val a = programs[indexA]
                        val b = programs[indexB]
                        programs[indexA] = b
                        programs[indexB] = a
                    }
                    it.startsWith("p") -> {
                        val a = action.split("/")[0]
                        val b = action.split("/")[1]
                        val indexA = programs.indexOf(a)
                        val indexB = programs.indexOf(b)
                        programs[indexA] = b
                        programs[indexB] = a
                    }
                }
            }
            if (programs == original) {
                val dancesLeft = (maxDances - dances).rem(dances)
                println("Repeats at $dances, only do $dancesLeft more")
                dances = maxDances - dancesLeft
            }
            dances++
        }
        programs.joinToString("")
    }
}
