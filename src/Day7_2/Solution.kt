package Day7_2

import solveInputs

data class Node(val name: String, private val weight: Int, private val childrenNames: List<String>) {

    private val children: List<Node> by lazy {
        childrenNames.map { nodesMap[it]!! }
    }

    private val totalWeight: Int by lazy {
        if (childrenNames.isEmpty()) {
            weight
        } else {
            children.map { it.totalWeight }.sum() + weight
        }
    }

    val depth: Int by lazy {
       if (childrenNames.isEmpty()) {
            0
        } else {
            children.map { it.depth }.max()!! + 1
        }
    }

    private fun areChildrenBalanced(): Boolean {
        val childWeights = children.map { it.totalWeight }.sorted()
        return childWeights.first() == childWeights.last()
    }

    fun findUnbalanced(correctWeight: Int = 0): Int {
        return if (areChildrenBalanced()) {
            val diff = correctWeight - totalWeight
            weight + diff
        } else {
            val sortedChildren = children.sortedBy { it.totalWeight }
            if (sortedChildren[0].totalWeight == sortedChildren[1].totalWeight) {
                sortedChildren.last().findUnbalanced(sortedChildren.first().totalWeight)
            } else {
                sortedChildren.first().findUnbalanced(sortedChildren.last().totalWeight)
            }
        }
    }

}

val nodes = mutableListOf<Node>()
val nodesMap = mutableMapOf<String, Node>()

fun main(args: Array<String>) {
    solveInputs { input ->
        nodes.clear()
        nodesMap.clear()
        input.lines().forEach {
            val name = it.split(" ")[0]
            val weight = it.split("(")[1].split(")")[0].toInt()
            val children: List<String> = if (it.contains("->")) {
                it.split("-> ")[1].split(", ").toList()
            } else {
                emptyList()
            }
            nodes += Node(name, weight, children)
        }
        nodesMap += nodes.associateBy { it.name }

        val root = nodes.maxBy { it.depth }!!
        val answer = root.findUnbalanced()
        "$answer"
    }
}
