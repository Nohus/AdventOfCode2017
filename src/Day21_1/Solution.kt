package Day21_1

import solveInputs

typealias Block = List<List<String>>

fun rotate(block: Block): Block {
    val rotated: Block
    val n = block.size
    if (n == 3) {
        rotated = listOf(mutableListOf("", "", ""), mutableListOf("", "", ""), mutableListOf("", "", ""))
    } else { // n == 2
        rotated = listOf(mutableListOf("", ""), mutableListOf("", ""))
    }
    for (i in 0 until n) {
        for (j in 0 until n) {
            rotated[i][j] = block[n - j - 1][i]
        }
    }
    return rotated
}

fun flipVertically(block: Block): Block {
    return block.reversed()
}

fun joinHorizontally(a: Block, b: Block): Block {
    val block = mutableListOf<List<String>>()
    for (row in 0 until a.size) {
        block += a[row] + b[row]
    }
    return block
}

fun joinVertically(a: Block, b: Block): Block {
    return a + b
}

fun printBlock(block: Block) {
    println(" ")
    block.forEach { row ->
        println(row.joinToString(""))
    }
}

fun transform(map: Map<Block, Block>, input: Block): Block {
    var block = input
    repeat(3) {
        if (map.containsKey(block)) return map[block]!!
        block = rotate(block)
    }
    if (map.containsKey(block)) return map[block]!!
    block = flipVertically(input)
    repeat(3) {
        if (map.containsKey(block)) return map[block]!!
        block = rotate(block)
    }
    if (map.containsKey(block)) return map[block]!!
    throw Exception("No transform found")
}

fun transformGrid(blockSize: Int, grid: Block, transforms: Map<Block, Block>, getBlockAt: (Int, Int) -> Block): Block {
    val blocks = mutableListOf<List<Block>>()
    for (y in 0 until grid.size step blockSize) {
        val blockRow = mutableListOf<Block>()
        for (x in 0 until grid.size step blockSize) {
            val block = getBlockAt(x, y)
            val outputBlock = transform(transforms, block)
            blockRow += outputBlock
        }
        blocks += blockRow
    }
    return blocks
            .map { it.reduce { a, b -> joinHorizontally(a, b) } }
            .reduce { a, b -> joinVertically(a, b) }
}

fun main(args: Array<String>) {
    solveInputs { input ->
        val transforms = mutableMapOf<Block, Block>()
        input.lines().forEach {
            val rule = it.split(" => ")[0]
            val result = it.split(" => ")[1]
            val inputBlock = rule.split("/").map { it.chunked(1) }
            val resultBlock = result.split("/").map { it.chunked(1) }
            transforms.put(inputBlock, resultBlock)
        }

        var grid = listOf(listOf(".", "#", "."), listOf(".", ".", "#"), listOf("#", "#", "#"))
        repeat(5) {
            if (grid.size % 2 == 0) {
                grid = transformGrid(2, grid, transforms) { x, y ->
                    listOf(
                            listOf(grid[y][x], grid[y][x + 1]),
                            listOf(grid[y + 1][x], grid[y + 1][x + 1])
                    )
                }
            } else if (grid.size % 3 == 0) {
                grid = transformGrid(3, grid, transforms) { x, y ->
                    listOf(
                            listOf(grid[y][x], grid[y][x + 1], grid[y][x + 2]),
                            listOf(grid[y + 1][x], grid[y + 1][x + 1], grid[y + 1][x + 2]),
                            listOf(grid[y + 2][x], grid[y + 2][x + 1], grid[y + 2][x + 2])
                    )
                }
            }
            printBlock(grid)
        }
        val answer = grid.map { it.count { it == "#" } }.sum()
        "$answer"
    }
}
