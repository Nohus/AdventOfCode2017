package Day15_1

import solveInputs

fun nextA(previous: Long): Long {
    return (previous * 16807L).rem(2147483647L)
}

fun nextB(previous: Long): Long {
    return (previous * 48271L).rem(2147483647L)
}

fun toBinary(number: Long): String {
    return String.format("%32s", java.lang.Long.toBinaryString(number)).replace(" ", "0")
}

fun matches(a: Long, b: Long): Boolean {
    return toBinary(a).takeLast(16) == toBinary(b).takeLast(16)
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var a = input.lines()[0].split(" ").last().toLong()
        var b = input.lines()[1].split(" ").last().toLong()
        var count = 0
        for (i in 1..40000000) {
            a = nextA(a)
            b = nextB(b)
            count += if (matches(a, b)) 1 else 0
        }
        "$count"
    }
}
