package Day16_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val moves = input.split(",")
        var programs = "abcdefghijklmnop".chunked(1).toMutableList()
        moves.forEach {
            val action = it.takeLast(it.length - 1)
            when {
                it.startsWith("s") -> {
                    val number = action.toInt() % programs.size
                    val newList = programs.takeLast(number) + programs.take(programs.size - number)
                    programs = newList.toMutableList()
                }
                it.startsWith("x") -> {
                    val indexA = action.split("/")[0].toInt()
                    val indexB = action.split("/")[1].toInt()
                    val a = programs[indexA]
                    val b = programs[indexB]
                    programs[indexA] = b
                    programs[indexB] = a
                }
                it.startsWith("p") -> {
                    val a = action.split("/")[0]
                    val b = action.split("/")[1]
                    val indexA = programs.indexOf(a)
                    val indexB = programs.indexOf(b)
                    programs[indexA] = b
                    programs[indexB] = a
                }
            }
        }
        programs.joinToString("")
    }
}
