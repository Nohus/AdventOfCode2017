package Day18_2

import solveInputs

var answer = 0

class Runner(private val lines: List<String>,
             private val map: MutableMap<String, Long>,
             private val inBuffer: MutableList<Long>,
             private val outBuffer: MutableList<Long>,
             private val id: Int) {

    private var pc = 0

    init {
        map["p"] = id.toLong()
    }

    private fun eval(value: String): Long {
        return try {
            value.toLong()
        } catch (e: NumberFormatException) {
            map.getValue(value)
        }
    }

    operator fun invoke(): Int {
        var executedLines = 0
        outer@ while (pc >= 0 && pc < lines.size) {
            val split = lines[pc].split(" ")
            when (split[0]) {
                "snd" -> {
                    outBuffer += eval(split[1])
                    if (id == 1) {
                        answer++
                    }
                }
                "set" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = y
                }
                "add" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = map.getValue(x) + y
                }
                "mul" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = map.getValue(x) * y
                }
                "mod" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = map.getValue(x) % y
                }
                "rcv" -> {
                    val x = split[1]
                    if (inBuffer.isNotEmpty()) {
                        map[x] = inBuffer.removeAt(0)
                    } else {
                        return executedLines
                    }
                }
                "jgz" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    if (eval(x) > 0) {
                        pc += y.toInt()
                        pc--
                    }
                }
            }
            executedLines++
            pc++
        }
        return executedLines
    }

}

fun main(args: Array<String>) {
    solveInputs { input ->
        val lines = input.lines()
        val mapA = mutableMapOf<String, Long>().withDefault { 0L }
        val mapB = mutableMapOf<String, Long>().withDefault { 0L }
        val bufferA = mutableListOf<Long>()
        val bufferB = mutableListOf<Long>()
        val programA = Runner(lines, mapA, bufferA, bufferB, 0)
        val programB = Runner(lines, mapB, bufferB, bufferA, 1)
        while (true) {
            val executedA = programA()
            val executedB = programB()
            if (executedA == 0 && executedB == 0) {
                break
            }
        }
        "$answer"
    }
}
