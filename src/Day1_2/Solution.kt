package Day1_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val numberList = input.chunked(1).map { it.toInt() }
        val halfSize = numberList.size / 2
        val sum = numberList.filterIndexed { index, number ->
            var checkIndex = index + halfSize
            if (checkIndex > numberList.lastIndex) {
                checkIndex -= numberList.size
            }
            number == numberList[checkIndex]
        }.sum()
        "$sum"
    }
}
