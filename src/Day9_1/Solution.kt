package Day9_1

import Day9_1.STATE.GARBAGE
import Day9_1.STATE.STREAM
import solveInputs

enum class STATE {
    STREAM, GARBAGE
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var level = 0
        var state = STREAM
        var total = 0
        var ignoreNext = false
        input.chunked(1).forEach {
            if (state == STREAM) {
                when (it) {
                    "{" -> {
                        level++
                        total += level
                    }
                    "}" -> {
                        level--
                    }
                    "," -> {}
                    "<" -> {
                        state = GARBAGE
                    }
                    else -> throw Exception(it)
                }
            } else if (state == GARBAGE) {
                if (ignoreNext) {
                    ignoreNext = false
                } else {
                    when (it) {
                        "!" -> {
                            ignoreNext = true
                        }
                        ">" -> {
                            state = STREAM
                        }
                    }
                }
            }
        }
        "$total"
    }
}
