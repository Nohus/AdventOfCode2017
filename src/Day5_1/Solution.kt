package Day5_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val numbers = input.lines().map { it.toInt() }.toMutableList()
        var index = 0
        var step = 0
        while (index >= 0 && index <= numbers.lastIndex) {
            step++
            val jump = numbers[index]
            numbers[index] += 1
            index += jump
        }
        "$step"
    }
}
