package Day23_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val map = mutableMapOf<String, Long>()
                .withDefault { 0 }
        fun getValue(name: String): Long {
            return try {
                name.toLong()
            } catch (e: NumberFormatException) {
                map.getValue(name)
            }
        }

        val lines = input.lines()
        var pc = 0
        var answer = 0
        while (pc >= 0 && pc < lines.size) {
            val line = lines[pc]
            val instruction = line.split(" ")[0]
            val a = line.split(" ")[1]
            val b = line.split(" ")[2]
            when (instruction) {
                "set" -> {
                    map.put(a, getValue(b))
                }
                "sub" -> {
                    map.put(a, getValue(a) - getValue(b))
                }
                "mul" -> {
                    map.put(a, getValue(a) * getValue(b))
                    answer++
                }
                "jnz" -> {
                    if (getValue(a) != 0L) {
                        pc += getValue(b).toInt() - 1
                    }
                }
            }
            pc++
        }
        "$answer"
    }
}
