package Day4_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        var validCount = 0
        input.lines().forEach {
            val set = mutableSetOf<List<Char>>()
            var valid = true
            it.split(" ").forEach {
                val normalized = it.toList().sorted()
                if (set.contains(normalized)) {
                    valid = false
                }
                set.add(normalized)
            }
            if (valid) {
                validCount++
            }
        }
        "$validCount"
    }
}
