package Day25_1

import Day25_1.TuringMachine.Direction.LEFT
import Day25_1.TuringMachine.Direction.RIGHT
import solveInputs

typealias State = String

class TuringMachine(
        initialState: State,
        initialTape: List<String>,
        private val transitions: List<Transition>
) {

    private var state = initialState
    private val tape = Tape(initialTape)

    companion object {

        fun parseMachineDescription(description: String): TuringMachine {
            val lines = description.lines()
            val initialState = lines[0].split(" ")[1]
            val initialTape = lines[1].split(" ")[1].split(",")
            val transitions = mutableListOf<Transition>()
            lines.drop(2).forEach {
                val sides = it.split(" -> ")
                val condition = sides[0]
                val action = sides[1]
                val state = condition.split(" ")[0]
                val readSymbol = condition.split(" ")[1]
                val writeSymbol = action.split(" ")[0]
                val moveDirection = if (action.split(" ")[1] == "L") LEFT else RIGHT
                val nextState = action.split(" ")[2]
                val transition = Transition(state, readSymbol, writeSymbol, moveDirection, nextState)
                transitions += transition
            }
            return TuringMachine(initialState, initialTape, transitions)
        }

    }

    enum class Direction {
        LEFT, RIGHT
    }

    class Tape(tape: List<String> = listOf("0")) {

        private val blankSymbol = "0"
        private val left = mutableListOf<String>()
        private val right = mutableListOf<String>()

        init {
            right += tape
        }

        fun move(direction: Direction) {
            when (direction) {
                LEFT -> {
                    if (left.isNotEmpty()) {
                        right.add(0, left.last())
                        left.removeAt(left.lastIndex)
                    } else {
                        right.add(0, blankSymbol)
                    }
                }
                RIGHT -> {
                    left.add(right.first())
                    right.removeAt(0)
                    if (right.isEmpty()) {
                        right += blankSymbol
                    }
                }
            }
        }

        fun read(): String {
            return right.first()
        }

        fun write(symbol: String) {
            right[0] = symbol
        }

        fun getContents(): List<String> {
            return left.toList() + right.toList()
        }

        fun print() {
            val leftString = (if (left.isNotEmpty()) " " else "") + left.joinToString("  ")
            val centerString = "${if (leftString.isNotEmpty()) " " else ""}[${right.first()}] "
            val rightString = right.takeLast(right.size - 1).joinToString("  ")
            println(leftString + centerString + rightString)
        }

    }

    data class Transition(
            val state: State,
            val readSymbol: String,
            val writeSymbol: String,
            val moveDirection: Direction,
            val nextState: State
    )

    fun run(steps: Int): Tape {
        repeat(steps) {
            val symbol = tape.read()
            val transition = transitions.first { it.readSymbol == symbol && it.state == state }
            tape.write(transition.writeSymbol)
            tape.move(transition.moveDirection)
            state = transition.nextState
        }
        return tape
    }

}

fun main(args: Array<String>) {
    solveInputs { input ->
        val machine = TuringMachine.parseMachineDescription(input)
        val tape = machine.run(12523873)
        val answer = tape.getContents().count { it == "1" }
        "$answer"
    }
}
