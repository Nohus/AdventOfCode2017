package Day4_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        var validCount = 0
        input.lines().forEach {
            val set = mutableSetOf<String>()
            var valid = true
            it.split(" ").forEach {
                if (set.contains(it)) {
                    valid = false
                }
                set.add(it)
            }
            if (valid) {
                validCount++
            }
        }
        "$validCount"
    }
}
