package Day3_1

import Day3_1.DIR.*
import solveInputs
import kotlin.math.abs

enum class DIR {

    RIGHT, UP, LEFT, DOWN;

    fun next(): DIR {
        return when (this) {
            RIGHT -> UP
            UP -> LEFT
            LEFT -> DOWN
            DOWN -> RIGHT
        }
    }

}

data class Pos(val x: Int, val y: Int) {

    fun to(dir: DIR): Pos {
        return when(dir) {
            RIGHT -> Pos(x + 1, y)
            UP -> Pos(x, y - 1)
            LEFT -> Pos(x - 1, y)
            DOWN -> Pos(x, y + 1)
        }
    }

}

fun main(args: Array<String>) {
    solveInputs { input ->
        val target = input.toInt()
        val grid = mutableMapOf<Pos, Int>()
        var pos = Pos(0, 0)
        var dir = RIGHT
        grid.put(pos, 1)
        for (number in 2..target) {
            pos = pos.to(dir)
            grid.put(pos, number)
            val spiralDir = dir.next()
            if (!grid.containsKey(pos.to(spiralDir))) {
                dir = spiralDir
            }
        }
        val valueMap = grid.entries.associateBy({ it.value }) { it.key }
        val targetPos = valueMap[target]!!
        val distance = abs(targetPos.x) + abs(targetPos.y)
        "$distance"
    }
}
