package Day14_1

import solveInputs

fun hash(input: String): String {
    val list = (0..255).toMutableList()
    val newList = mutableListOf<Int>()
    var current = 0
    var skipSize = 0
    val bytes = input.toCharArray().map { it.toByte() }.toMutableList()
    bytes.addAll(listOf(17, 31, 73, 47, 23))
    for (i in 1..64) {
        bytes.forEach { length ->
            newList.clear()
            val endIndex = current + length
            if (endIndex > list.size) {
                val overflow = endIndex - list.size
                val reversed = (list.subList(current, list.size) + list.subList(0, overflow)).reversed()
                newList.apply {
                    addAll(reversed.subList(reversed.size - overflow, reversed.size)) // End of reversed fragment
                    addAll(list.subList(overflow, current)) // Between
                    addAll(reversed.subList(0, reversed.size - overflow)) // Start of reversed fragment
                }
            } else {
                newList.apply {
                    addAll(list.subList(0, current)) // Before reversed fragment
                    addAll(list.subList(current, current + length).reversed()) // Reversed
                    addAll(list.subList(current + length, list.size)) // After reversed fragment
                }
            }
            list.clear()
            list.addAll(newList)
            current += (length + skipSize)
            current -= list.size * (current / list.size)
            skipSize++
        }
    }
    return list.chunked(16).map { block ->
        block.takeLast(15).fold(block[0], { result, next -> result xor next})
    }.joinToString("") { String.format("%02x", it) }
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var sum = 0
        for (i in 0..127) {
            val hash = hash("$input-$i")
            val binHash = hash.chunked(1)
                    .map { String.format("%4s", Integer.toBinaryString(it.toInt(16)))}
                    .map { it.replace(" ", "0") }
                    .joinToString("")
            val used = binHash.chunked(1).count { it == "1" }
            sum += used
        }
        "$sum"
    }
}
