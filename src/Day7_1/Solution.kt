package Day7_1

import solveInputs

data class Node(val name: String, private val weight: Int, private val childrenNames: List<String>) {

    val depth: Int by lazy {
        if (childrenNames.isEmpty()) {
            0
        } else {
            childrenNames.map { nodesMap[it]!! }.map { it.depth }.max()!! + 1
        }
    }

}

val nodes = mutableListOf<Node>()
val nodesMap = mutableMapOf<String, Node>()

fun main(args: Array<String>) {
    solveInputs { input ->
        input.lines().forEach {
            val name = it.split(" ")[0]
            val weight = it.split("(")[1].split(")")[0].toInt()
            val children: List<String> = if (it.contains("->")) {
                it.split("-> ")[1].split(", ").toList()
            } else {
                emptyList()
            }
            nodes += Node(name, weight, children)
        }
        nodesMap += nodes.associateBy { it.name }

        val root = nodes.maxBy { it.depth }!!
        root.name
    }
}
