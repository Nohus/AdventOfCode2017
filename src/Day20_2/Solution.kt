package Day20_2

import solveInputs

data class Vector(
        val x: Long,
        val y: Long,
        val z: Long
) {

    operator fun plus(other: Vector): Vector {
        return Vector(x + other.x, y + other.y, z + other.z)
    }

}

data class Particle(
        var position: Vector,
        var velocity: Vector,
        var acceleration: Vector
) {

    fun tick() {
        velocity += acceleration
        position += velocity
    }

}

fun toVector(text: String): Vector {
    val list = text
            .substringAfter("<")
            .substringBefore(">")
            .split(",")
            .map { it.toLong() }
    return Vector(list[0], list[1], list[2])
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var particles = input.lines().map {
            val vectors = it.split(", ")
                    .map { toVector(it) }
            return@map Particle(vectors[0], vectors[1], vectors[2])
        }
        for (i in 1..1000) {
            particles.forEach { it.tick() }
            particles = particles.groupBy { it.position }
                    .filter { it.value.size == 1 }
                    .map { it.value[0] }
        }
        "${particles.size}"
    }
}
