package Day18_1

import solveInputs

val map = mutableMapOf<String, Long>().withDefault { 0L }

fun eval(value: String): Long {
    return try {
        value.toLong()
    } catch (e: NumberFormatException) {
        map.getValue(value)
    }
}

fun main(args: Array<String>) {
    solveInputs { input ->
        map.clear()
        val lines = input.lines()
        var i = 0
        var lastPlayed: Long? = null
        outer@ while (i < lines.size) {
            val split = lines[i].split(" ")
            when (split[0]) {
                "snd" -> {
                    val x = eval(split[1])
                    lastPlayed = x
                }
                "set" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = y
                }
                "add" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = map.getValue(x) + y
                }
                "mul" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = map.getValue(x) * y
                }
                "mod" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    map[x] = map.getValue(x) % y
                }
                "rcv" -> {
                    val x = eval(split[1])
                    if (x != 0L) {
                        break@outer
                    }
                }
                "jgz" -> {
                    val x = split[1]
                    val y = eval(split[2])
                    if (map.getValue(x) > 0) {
                        i += y.toInt()
                        i--
                    }
                }
            }
            i++
        }
        "$lastPlayed"
    }
}
