package Day13_1

import solveInputs

val ranges = mutableMapOf<Int, Int>()
        .withDefault { 0 }
val scanners = mutableMapOf<Int, Int>()
val scannerDirections = mutableMapOf<Int, Boolean>()

fun advanceScanners() {
    for ((layer, position) in scanners) {
        var newPosition: Int
        if (scannerDirections[layer]!!) {
            newPosition = position + 1
            if (newPosition == ranges.getValue(layer) - 1) {
                scannerDirections[layer] = false
            }
        } else {
            newPosition = position - 1
            if (newPosition == 0) {
                scannerDirections[layer] = true
            }
        }
        scanners.put(layer, newPosition)
    }
}

fun main(args: Array<String>) {
    solveInputs { input ->
        input.lines().forEach {
            val split = it.split(": ")
            val layer = split[0].toInt()
            val range = split[1].toInt()
            ranges.put(layer, range)
            scanners.put(layer, 0)
            scannerDirections.put(layer, true)
        }
        val maxDepth = ranges.keys.max()!!
        var severitySum = 0
        for (i in 0..maxDepth) {
            val severity = if (scanners.getOrDefault(i, -1) == 0) i * ranges.getValue(i) else 0
            severitySum += severity
            advanceScanners()
        }
        "$severitySum"
    }
}
