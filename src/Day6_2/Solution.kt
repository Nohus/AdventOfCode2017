package Day6_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val numbers = input.split("\t").map { it.toInt() }.toMutableList()
        val states = mutableSetOf<List<Int>>()
        states += numbers

        var loopCounting = false
        while (true) {
            val highestIndex = numbers.indexOf(numbers.max())
            var value = numbers[highestIndex]
            numbers[highestIndex] = 0
            var distributionIndex = highestIndex + 1
            while (value > 0) {
                if (distributionIndex > numbers.lastIndex) {
                    distributionIndex = 0
                }
                numbers[distributionIndex]++
                value--
                distributionIndex++
            }
            if (numbers in states) {
                if (!loopCounting) {
                    loopCounting = true
                    states.clear()
                    states += numbers
                } else {
                    break
                }
            } else {
                states += numbers
            }
        }
        "${states.size}"
    }
}
