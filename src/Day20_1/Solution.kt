package Day20_1

import solveInputs
import kotlin.math.abs

data class Vector(
        val x: Long,
        val y: Long,
        val z: Long
) {

    fun distance(): Long {
        return abs(x) + abs(y) + abs(z)
    }

}

data class Particle(
        val id: Int,
        var position: Vector,
        var velocity: Vector,
        var acceleration: Vector
)

fun toVector(text: String): Vector {
    val list = text
            .substringAfter("<")
            .substringBefore(">")
            .split(",")
            .map { it.toLong() }
    return Vector(list[0], list[1], list[2])
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var id = -1
        val particles = input.lines().map {
            val vectors = it.split(", ")
                    .map { toVector(it) }
            id++
            return@map Particle(id, vectors[0], vectors[1], vectors[2])
        }
        val particle = particles.minBy { it.acceleration.distance() }!!
        "${particle.id}"
    }
}
