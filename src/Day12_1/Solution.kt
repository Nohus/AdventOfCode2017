package Day12_1

import solveInputs

val pipes = mutableMapOf<Int, MutableSet<Int>>()

fun addConnection(a: Int, b: Int) {
    val list = pipes[a] ?: mutableSetOf()
    list += b
    pipes[a] = list
}

fun main(args: Array<String>) {
    solveInputs { input ->
        pipes.clear()
        input.lines().forEach {
            val split = it.split(" <-> ")
            val number = split[0].toInt()
            split[1].split(", ").map(String::toInt).forEach {
                addConnection(number, it)
            }
        }
        val expanded = mutableSetOf(0)
        val connections = pipes[0]!!
        while (connections.isNotEmpty()) {
            connections.toSet().forEach {
                pipes[it]!!.filterNot { it in expanded }.forEach {
                    connections += it
                }
                expanded += it
                connections -= it
            }
        }
        "${expanded.size}"
    }
}
