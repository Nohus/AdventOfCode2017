package Day3_2

import Day3_2.DIR.*
import solveInputs

enum class DIR {

    RIGHT, UP, LEFT, DOWN;

    fun next(): DIR {
        return when (this) {
            RIGHT -> UP
            UP -> LEFT
            LEFT -> DOWN
            DOWN -> RIGHT
        }
    }

}

data class Pos(val x: Int, val y: Int) {

    fun to(dir: DIR): Pos {
        return when(dir) {
            RIGHT -> Pos(x + 1, y)
            UP -> Pos(x, y - 1)
            LEFT -> Pos(x - 1, y)
            DOWN -> Pos(x, y + 1)
        }
    }

}

fun neighbourSum(pos: Pos): Int {
    var sum = 0
    for (x in -1..1) {
        for (y in -1..1) {
            val neighbour = Pos(pos.x + x, pos.y + y)
            sum += grid[neighbour] ?: 0
        }
    }
    return sum
}

val grid = mutableMapOf<Pos, Int>()

fun main(args: Array<String>) {
    solveInputs { input ->
        val target = input.toInt()
        var pos = Pos(0, 0)
        var dir = RIGHT
        grid.put(pos, 1)
        val answer: Int?
        while (true) {
            pos = pos.to(dir)
            val sum = neighbourSum(pos)
            if (sum > target) {
                answer = sum
                break
            }
            grid.put(pos, sum)
            dir.next().let {
                if (!grid.containsKey(pos.to(it))) {
                    dir = it
                }
            }
        }
        "$answer"
    }
}
