package Day17_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val forward = input.toInt()
        var bufferSize = 1
        var current = 0
        var value = 0
        for (i in 1..50000000) {
            current += forward
            current %= bufferSize
            bufferSize++
            if (current == 0) {
                value = i
            }
            current += 1
        }
        "$value"
    }
}
