package Day23_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs {
        // Code manually translated and optimized from provided assembly
        var b = 109900
        val c = b + 17000
        var isPrime = true
        var answer = 0
        while (true) {
            for (i in 2..(b - 1)) {
                if (b % i == 0) {
                    isPrime = false
                    break
                }
            }
            if (!isPrime) {
                answer++
            }
            if (b != c) {
                b += 17
                isPrime = true
            } else {
                break
            }
        }
        "$answer"
    }
}
