package Day9_2

import Day9_2.STATE.GARBAGE
import Day9_2.STATE.STREAM
import solveInputs

enum class STATE {
    STREAM, GARBAGE
}

fun main(args: Array<String>) {
    solveInputs { input ->
        var state: STATE = STREAM
        var total = 0
        var ignore = false
        input.chunked(1).forEach {
            if (state == STREAM) {
                when (it) {
                    "{" -> {}
                    "}" -> {}
                    "," -> {}
                    "<" -> {
                        state = GARBAGE
                    }
                    else -> throw Exception(it)
                }
            } else if (state == GARBAGE) {
                if (ignore) {
                    ignore = false
                } else {
                    when (it) {
                        "!" -> {
                            ignore = true
                        }
                        ">" -> {
                            state = STREAM
                        }
                        else -> {
                            total++
                        }
                    }
                }
            }
        }
        "$total"
    }
}
