package Day15_2

import solveInputs

class Generator(private val factor: Long, private val multiple: Int) {

    operator fun invoke(previous: Long): Long {
        var candidate = previous
        do {
            candidate = (candidate * factor).rem(2147483647L)
        } while (candidate.rem(multiple) != 0L)
        return candidate
    }

}

fun toBinary(number: Long): String {
    return String.format("%32s", java.lang.Long.toBinaryString(number)).replace(" ", "0")
}

fun matches(a: Long, b: Long): Boolean {
    return toBinary(a).takeLast(16) == toBinary(b).takeLast(16)
}

fun main(args: Array<String>) {
    solveInputs { input ->
        val nextA = Generator(16807L, 4)
        val nextB = Generator(48271L, 8)
        var a = input.lines()[0].split(" ").last().toLong()
        var b = input.lines()[1].split(" ").last().toLong()
        var count = 0
        for (i in 1..5000000) {
            a = nextA(a)
            b = nextB(b)
            if (matches(a, b)) {
                count++
            }
        }
        "$count"
    }
}
