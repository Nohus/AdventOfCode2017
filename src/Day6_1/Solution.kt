package Day6_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val numbers = input.split("\t").map { it.toInt() }.toMutableList()
        val states = mutableSetOf<List<Int>>()
        states += numbers

        while (true) {
            val highestIndex = numbers.indexOf(numbers.max())
            var value = numbers[highestIndex]
            numbers[highestIndex] = 0
            var distributionIndex = highestIndex + 1
            while (value > 0) {
                if (distributionIndex > numbers.lastIndex) {
                    distributionIndex = 0
                }
                numbers[distributionIndex]++
                value--
                distributionIndex++
            }
            if (numbers in states) {
                break
            } else {
                states += numbers
            }
        }
        "${states.size}"
    }
}
