package Day19_1

import Day19_1.Dir.*
import solveInputs

enum class Dir {
    UP, RIGHT, DOWN, LEFT
}

fun main(args: Array<String>) {
    solveInputs { input ->
        val grid = mutableListOf<List<String>>()
        input.lines().forEach {
            grid += it.chunked(1)
        }
        var x = grid[0].indexOf("|")
        var y = 0
        var dir = DOWN
        val list = mutableListOf<String>()
        try {
            while (true) {
                when (dir) {
                    UP -> y -= 1
                    RIGHT -> x += 1
                    LEFT -> x -= 1
                    DOWN -> y += 1
                }
                val symbol = grid[y][x]
                when (symbol) {
                    "|" -> {}
                    "-" -> {}
                    "+" -> {
                        if (grid.getOrNull(y + 1)?.getOrNull(x) ?: " " == "|" && dir != UP) {
                            dir = DOWN
                        } else if (grid.getOrNull(y - 1)?.getOrNull(x) ?: " " == "|" && dir != DOWN) {
                            dir = UP
                        } else if (grid.getOrNull(y)?.getOrNull(x + 1) ?: " " == "-" && dir != LEFT) {
                            dir = RIGHT
                        } else if (grid.getOrNull(y)?.getOrNull(x - 1) ?: " " == "-" && dir != RIGHT) {
                            dir = LEFT
                        }
                    }
                    else -> list += symbol
                }
            }
        } catch (e: IndexOutOfBoundsException) {}
        list.joinToString("")
    }
}
