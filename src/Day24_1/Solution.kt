package Day24_1

import solveInputs

data class Component(val portA: Int, val portB: Int) {

    override fun toString(): String {
        return "$portA/$portB"
    }

}

class Bridge(private val firstComponent: Component, var freeComponents: MutableList<Component>) {

    private var components = mutableListOf<Component>()

    var port: Int
    private set

    init {
        components.add(firstComponent)
        freeComponents.remove(firstComponent)
        port = if (firstComponent.portA == 0) firstComponent.portB else firstComponent.portA
    }

    fun copy(): Bridge {
        val new = Bridge(firstComponent, freeComponents)
        new.freeComponents = freeComponents.toMutableList()
        new.components = components.toMutableList()
        new.port = port
        return new
    }

    fun addComponent(component: Component) {
        port = when (port) {
            component.portA -> component.portB
            component.portB -> component.portA
            else -> throw Exception("Invalid component")
        }
        components.add(component)
        freeComponents.remove(component)
    }

    fun getStrength(): Int {
        return components.sumBy { it.portA + it.portB }
    }

    override fun toString(): String {
        return components.joinToString(" - ")
    }

}

fun main(args: Array<String>) {
    solveInputs { input ->
        val components = mutableListOf<Component>()
        val completeBridges = mutableListOf<Bridge>()
        val bridges = mutableListOf<Bridge>()
        input.lines().forEach {
            components += Component(it.split("/")[0].toInt(), it.split("/")[1].toInt())
        }
        components.filter { it.portA == 0 || it.portB == 0 }.forEach {
            bridges += Bridge(it, components.toMutableList())
        }

        while (bridges.isNotEmpty()) {
            val bridge = bridges.first()
            val port = bridge.port
            val matchingComponents = bridge.freeComponents.filter { it.portA == port || it.portB == port }
            if (matchingComponents.isEmpty()) {
                bridges -= bridge
                completeBridges += bridge
            } else {
                matchingComponents.forEach {
                    val newBridge = bridge.copy()
                    newBridge.addComponent(it)
                    bridges += newBridge
                }
                bridges -= bridge
            }
        }
        println("Constructed all possible bridges")
        val strongestBridge = completeBridges.maxBy { it.getStrength() }!!
        println("Strongest bridge: $strongestBridge")

        "${strongestBridge.getStrength()}"
    }
}
