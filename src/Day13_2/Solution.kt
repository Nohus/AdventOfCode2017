package Day13_2

import solveInputs

val multiples = mutableMapOf<Int, Int>()

fun isCaught(delay: Int): Boolean {
    for ((layer, multiple) in multiples) {
        if ((delay + layer).rem(multiple) == 0) {
            return true
        }
    }
    return false
}

fun main(args: Array<String>) {
    solveInputs { input ->
        multiples.clear()
        input.lines().forEach {
            val split = it.split(": ")
            val layer = split[0].toInt()
            val range = split[1].toInt()
            val repeat = (range - 1) * 2
            multiples.put(layer, repeat)
        }
        var delay = -1
        while (isCaught(++delay));
        "$delay"
    }
}
