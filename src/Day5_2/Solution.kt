package Day5_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val numbers = input.lines().map { it.toInt() }.toMutableList()
        var index = 0
        var step = 0
        while (index >= 0 && index <= numbers.lastIndex) {
            step++
            val jump = numbers[index]
            if (jump >= 3) {
                numbers[index] -= 1
            } else {
                numbers[index] += 1
            }
            index += jump
        }
        "$step"
    }
}
