package Day8_1

import solveInputs

val map = mutableMapOf<String, Int>()
        .withDefault { 0 }

fun main(args: Array<String>) {
    solveInputs { input ->
        map.clear()
        input.lines().forEach {
            val ins = it.substringBefore(" if ")
            val condition = it.substringAfter(" if ")

            val register = ins.split(" ").first()
            val op = ins.split(" ")[1]
            val amount = ins.split(" ").last().toInt()
            val checkRegister = condition.split(" ").first()
            val check = condition.split(" ")[1]
            val checkValue = condition.split(" ").last().toInt()

            val checkActual = map.getValue(checkRegister)
            val isTrue = when (check) {
                ">" -> { checkActual > checkValue }
                "<" -> { checkActual < checkValue }
                ">=" -> { checkActual >= checkValue }
                "<=" -> { checkActual <= checkValue }
                "==" -> { checkActual == checkValue }
                "!=" -> { checkActual != checkValue }
                else -> throw Exception()
            }
            if (isTrue) {
                when (op) {
                    "inc" -> { map[register] = map.getValue(register) + amount }
                    "dec" -> { map[register] = map.getValue(register) - amount }
                    else -> throw Exception()
                }
            }
        }

        val greatest = map.values.max()
        "$greatest"
    }
}
