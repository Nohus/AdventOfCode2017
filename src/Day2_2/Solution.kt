package Day2_2

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val sum = input.lines().map {
            val list = it.split("\t").map { it.toInt() }
            var number = 0
            list.forEachIndexed { indexA, a ->
                list.forEachIndexed { indexB, b ->
                    if (indexA != indexB) {
                        if (a.rem(b) == 0) {
                            number = a / b
                        }
                    }
                }
            }
            return@map number
        }.sum()
        "$sum"
    }
}
