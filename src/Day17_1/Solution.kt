package Day17_1

import solveInputs

fun main(args: Array<String>) {
    solveInputs { input ->
        val forward = input.toInt()
        val buffer = mutableListOf<Int>()
        buffer.add(0)
        var current = 0
        for (i in 1..2017) {
            current += forward
            current %= buffer.size
            current += 1
            buffer.add(current, i)
        }
        "${buffer[current + 1]}"
    }
}
